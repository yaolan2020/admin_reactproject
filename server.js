const express = require('express'),
bodyParser = require('body-parser'),
mysql = require('mysql');

let server = express();

//解决跨域问题
server.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");//项目上线后改成页面的地址
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    next();
})
//监听端口
server.listen(3333,() => {
    console.log('app listening at port 3333')
});
//路由
let userRouter = express.Router();
let newsRouter = express.Router();
server.use('/user',userRouter);
server.use('/news',newsRouter);
userRouter.use('/yaolan',function(req,res){
    res.send('yaolan')
})
newsRouter.use('/hot',function(req,res){
    res.send('新闻')
})
//post请求数据加工
server.use(bodyParser.urlencoded({}));   

// server.get('',function(req,res){
//      console.log('有人get了')
//      console.log(req.query)
//      res.send({name:'yaolan',age:23})
// })
// server.post('',function(req,res){
//     console.log('有人post了')
//     console.log(req.body)
//     res.send({name:'刘文龙',age:23})
// })


server.use('/getText',function(req,res){
     let pool = mysql.createPool({
         'host':'localhost',
         'user':'root',
         'password':'yl094551',
         'database':'reactproject',
         'insecureAuth' : true
     });
     pool.getConnection(function(err, connection) {
        if(err){
            console.log(err)
        }else{
            connection.query("INSERT INTO `newstable` (`user`,`textName`,`time`,`inner`) VALUES('"+req.query.user+"','"+req.query.textName+"','"+req.query.time+"','"+req.query.inner+"')",function(err,data){
                if(err){
                    res.send({code:404,msg:err})
                }else{
                    connection.end();
                    res.send({code:0})
                }
            })
        }
      });
})
server.use('/getUser',function(req,res){
    let pool = mysql.createPool({
        'host':'localhost',
        'user':'root',
        'password':'yl094551',
        'database':'reactproject',
        'insecureAuth' : true
    });
    pool.getConnection(function(err, connection) {
       if(err){
           console.log(err)
       }else{
        let resule={};
        connection.query("select u.userName,u.position,u.workAddr,u.birthDate,u.university,u.education,u.major,u.workDate,s.sexCode,s.sexLabel from usertable u left join sextable s on u.userSex=s.sexCode  WHERE u.userName='姚兰'",function(err,data){
            if(err){
                resule={
                    code:404,
                    msg:err
                }
                res.send(resule)
            }else{
                resule={
                    code:0,
                    data:data[0]
                }
                connection.query("select w.companyName,w.projectDes,w.workDateStart,w.workDateEnd  from work_experience w  WHERE w.userName='姚兰'",function(err2,data2){
                    if(err2){
                        if(resule.code === 0){
                            resule.data.workexprience=[];
                        }else{
                            console.log(err2)
                        }
                    }else{
                        resule.data.workexprience=data2
                    }
                    connection.end();
                    res.send(resule)
                })
            }
        })
       }
     });
})
// server.use(static(''))    



